#
# lupus Makefile
#
# Copyright 2014 Mònica Ramírez Arceda <monica@probeta.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

API_NAME='lupus'
CGI_BIN_DIR='/www/cgi-bin'
LIB_DIR='/usr/lib/lua/lupus'
CONFIG_DIR='/etc/config'
HOST=192.168.1.1
USER=root
DEST_DIR=/

install-files:
	mkdir -p ${DEST_DIR}/${CGI_BIN_DIR}
	mkdir -p ${DEST_DIR}/${LIB_DIR}
	mkdir -p ${DEST_DIR}/${CONFIG_DIR}
	cp src/cgi/${API_NAME} ${DEST_DIR}/${CGI_BIN_DIR}
	chmod +x ${DEST_DIR}/${CGI_BIN_DIR}/${API_NAME}
	cp -r `find src/* -not -iwholename "src/cgi*"` ${DEST_DIR}/${LIB_DIR}
	cp etc/config/${API_NAME} ${DEST_DIR}/${CONFIG_DIR}

install-dependencies:
	opkg update
	opkg install luafilesystem

install: install-files install-dependencies
	
uninstall:
	rm -r ${LIB_DIR}
	rm ${CGI_BIN_DIR}/${API_NAME}
	rm ${CONFIG_DIR}/${API_NAME}
	
install-remote:
	ssh ${USER}@${HOST} 'mkdir -p ${LIB_DIR}'
	scp src/cgi/${API_NAME} ${USER}@${HOST}:${CGI_BIN_DIR}
	ssh ${USER}@${HOST} 'chmod +x ${CGI_BIN_DIR}/${API_NAME}'
	scp -r `find src/* -not -iwholename "src/cgi*"` ${USER}@${HOST}:${LIB_DIR}
	scp etc/config/${API_NAME} ${USER}@${HOST}:${CONFIG_DIR}
	ssh ${USER}@${HOST} 'opkg update && opkg install luafilesystem'
	
uninstall-remote:
	ssh ${USER}@${HOST} 'rm -r ${LIB_DIR} && rm ${CGI_BIN_DIR}/${API_NAME} && rm ${CONFIG_DIR}/${API_NAME}'
	
gen-doc:
	markdown doc/lupus-doc.markdown > doc/lupus-doc.html
