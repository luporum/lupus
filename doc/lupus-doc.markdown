LUPUS API DOCUMENTATION
=======================

**UCI**

* [Add: POST /uci/add](#add)
* [Delete: POST /uci/delete](#delete)
* [Get: GET /uci/get](#get)
* [Get all: GET /uci/get_all](#get_all)
* [Get configs: GET /uci/get_configs](#get_configs)
* [Set: POST /uci/set](#set)

**System**

* [Reboot the system: GET /system/reboot](#reboot)
* [Restart a service: GET /system/restart_service](#restart_service)
* [Start a service: GET /system/start_service](#start_service)
* [Stop a service: GET /system/stop_service](#stop_service)

**Authentication**

* [None](#none)
* [Password: POST](#password)

<a id="add"></a>
Add: POST /uci/add
------------------

Add one or more anonymous sections of the given types to the given configs.

*POST data:*

	{
		"uci": {
	  		"config1.type1"[,
	  		"config2.type2",
	  		...]
	  	}
	}

*Error codes:*

* 400 (Bad Request): If request format is not valid JSON format.

*Example*

POST data:

	{
		"uci": {
  			"network.type1",
  			"system.type2"
  		}
	}
	
This will produce the following result:

	# cat /etc/config/network:
	...
	config type1

	# cat /etc/config/system:
	...
	config type2

<a id="delete"></a>
Delete: POST /uci/delete
------------------------

Delete sections or options of a config.

*POST data*

To delete a whole section:

	{
		"uci": {
  			"config.section"
  		}
	}

To delete an option of a section:

	{
		"uci": {
  			"config.section.option"
  		}
	}

*Error codes*

* 400 (Bad Request): If request format is not valid JSON format

*Examples*

POST data:

		{
			"uci": {
  				"network.@interface[1]"
  			}
		}

will remove the second section of type interface of network config file.

POST data:

		{
			"uci": {
  				"network.@interface[1].ipaddr"
  			}
		}

will remove ipaddr option of the second section of type interface of network config file.

<a id="get"></a>
Get: GET /uci/get
-----------------

Get the value of the given option or the type of the given section.

*GET data:*

To get an option's value:

	/uci/get/config.section.option

To get a type's section:

	/uci/get/config.section

Output format:

	{
  		"config.section.option":"value"
	}

or

	{
  		"config.section":"type"
	}

*Error codes*

 * 404 (Not found): If config.section.option does not exist.

*Examples*

Request:

	/uci/get/system.ntp.enable_server

Response:

		{
 			"system.ntp.enable_server":"0"
		}

Request:

	/uci/get/system.ntp.server

Response:

		{
			"system.ntp.server":["0.openwrt.pool.ntp.org","1.openwrt.pool.ntp.org","2.openwrt.pool.ntp.org","3.openwrt.pool.ntp.org"]
		}

Request:

	/uci/get/system.@system[0].hostname

Response:

		{
			"system.@system[0].hostname":"OpenWrt"
		}

Request:

	/uci/get/network.lan.ipaddr

Response:

		{
  			"network.lan.ipaddr":"192.168.1.5"
  		}

Request:

	/uci/get/network.@interface[2].ipaddr

Response:

  		{
  			"network.@interface[2].ipaddr":"192.168.1.5"
  		}

Request:

	/uci/get/network.loopback

Response:

  		{
  			"network.loopback":"interface"
  		}

<a id="get_all"></a>
Get all GET /uci/get_all
------------------------

Get all sections and options for the given config or for all configs if no config is given.

*Output format*

If no config is given (/uci/get_all):

	{
		"config1":{
    		"section1":{
      			".name":"section1",
      			".type":"section_type",
      			".index":0,
      			".anonymous":true|false,
      			"option1":"value1",
      			"option2":"value2",
      		},
      		"section2":{
      			".name":"sectin2",
      			".type":"section_type",
      			".index":1,
      			".anonymous":true|false,
      			"option1":"value1",
      			"option2":"value2",
      		},
      		...
      	},
      	"config2":{
      		...
      	},
      	...
	}

If a config is given (/uci/get_all/config):

	{
		"section1":{
      		".name":"section1",
      		".type":"section_type",
      		".index":0,
      		".anonymous":true|false,
      		"option1":"value1",
      		"option2":"value2",
      	},
      	"section2":{
      		".name":"sectin2",
      		".type":"section_type",
      		".index":1,
      		".anonymous":true|false,
      		"option1":"value1",
      		"option2":"value2",
      	},
      	...
	}

*Error codes*

* 404 (Not found): If config does not exist.

*Example*

Request: /uci/get_all/system

Response format:

		{
	  		"ntp":{
	    		".name":"ntp",
	    		".type":"timeserver",
	    		".index":1,
	    		"enable_server":"0",
	    		".anonymous":false,
	    		"server":["0.openwrt.pool.ntp.org","1.openwrt.pool.ntp.org","2.openwrt.pool.ntp.org","3.openwrt.pool.ntp.org"]
	  		},
	  		"cfg02e48a":{
	    		".name":"cfg02e48a",
	    		".type":"system",
	    		"hostname":"OpenWrt",
	    		".index":0,
	    		".anonymous":true,
	    		"timezone":"UTC"
	  		}
		}

<a id="get_configs"></a>
Get configs GET /uci/get_configs
--------------------------------

Return file names in /etc/config
	
Response format:

	{
		"configs":["filename1","filename2",...]
	}

*Example*

Response:

		{
			"configs":["luci","network","firewall","dhcp","system","ucitrack","uhttpd","dropbear"]
		}

<a id="set"></a>
Set POST /uci/set
-----------------

If an option name is given, set values of given options. If the option does not exist, it is added.

If no option name is given, add new sections with the type set to the given value.

*POST data*

	{
		"uci": {
  			"config.section.option":"value",
			"config.type":"sectionname",
			...
		}
	}
	

*Error codes*

* 400 (Bad Request): If request format is not valid JSON format

*Examples*

		{
			"uci": {
  				"network.loopback.ipaddr": "127.0.0.1"
  				"system.@system[0].hostname": "OpenWrt"
	  			"system.ntp.server":["0.openwrt.pool.ntp.org","1.openwrt.pool.ntp.org","2.openwrt.pool.ntp.org","3.openwrt.pool.ntp.org"]
	  			"system.@system[0].log_file": "/var/log/messages",
				"network.interface":"wlan0"
				"network.wlan0.ipaddr":"192.168.1.11"
			}
		}

This will produce the following result:

	# cat /etc/config/network:
	...
	config interface loopback
		option ipaddr '127.0.0.1'

	config interface wlan0
		option ipaddr '192.168.1.11'

	# cat /etc/config/system:
	config system
		...
		option hostname 'OpenWrt'
		option log_file '/var/log/messages'

	config timeserver 'ntp'
		...
		list server '0.openwrt.pool.ntp.org'
		list server '1.openwrt.pool.ntp.org'
		list server '2.openwrt.pool.ntp.org'
		list server '3.openwrt.pool.ntp.org'

<a id="reboot"></a>
Reboot GET /system/reboot
-------------------------

Reboot the system.

*GET data:*

	/system/reboot

*Error codes*

* 400 (Bad Request): If params are given.

<a id="restart_service"></a>
Restart a service GET /system/restart_service
---------------------------------------------

Restarts the given service.

*GET data:*

	/system/restart_service/service_name

*Error codes*

* 400 (Bad Request): If service_name does not exist

*Examples*

Request:

	/system/restart_service/firewall

<a id="start_service"></a>
Sstart a service GET /system/start_service
------------------------------------------

Starts the given service.

*GET data:*

	/system/start_service/service_name

*Error codes*

* 400 (Bad Request): If service_name does not exist

*Examples*

Request:

	/system/start_service/firewall

<a id="stop_service"></a>
Stop a service GET /system/stop_service
---------------------------------------

Stops the given service.

*GET data:*

	/system/stop_service/service_name

*Error codes*

* 400 (Bad Request): If service_name does not exist

*Examples*

Request:

	/system/stop_service/firewall

<a id="none"></a>
None
----

No authentication is needed. You don't have to add anything to POST data

<a id="password"></a>
Password: POST
--------------

A password must be provided to use the API.

*POST data:*

	{ "auth":
		{
        	"password":"lupuspassword"}
		}
	}

*Error codes:*

* 401 (Unauthorized): If password is not the right one.

