--[[
lupus - Controller

Description:
Receives a request froma CGI and sends a reponse

License:
Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--
local json = require ("lupus.libs.dkjson")
local validator = require ("lupus.core.Validator")
local constants = require ("lupus.core.constants")
local strings = require("lupus.utils.strings")

local auth_controller = require("lupus.modules.auth.AuthController")
local uci_controller = require("lupus.modules.uci.UCIController")
local system_controller = require("lupus.modules.system.SystemController")

Controller = {}
Controller.__index = Controller

--- Controller constructor
-- Creates a new Controller
function Controller.new()
	local ctrl = {}
	setmetatable(ctrl,Controller)
	ctrl.request_get = ""
	ctrl.request_post = ""
	return ctrl
end

--- Get GET request and convert it to a table
-- @return The indexed table {"str1","str2",...}
local function request_get()
	local pathinfo = os.getenv("PATH_INFO") or ""
	local t = strings.split_char(pathinfo,"/")
	return t
end

--- Get POST Request
-- @return POST data if there is data sent via POST,
--         an error string if POST data is not well-formed
--         or nil if there is no POST data
local function request_post()
	-- Check if there is POST data
	local request_method = os.getenv("REQUEST_METHOD") or ""
	if (request_method == "POST") then
		-- Read all POST data
		local post_data = io.read("*all")
		-- Decode POST data, it must be in JSNO format
		local obj, pos, err = json.decode(post_data, 1, nil)
		-- If there is an error, return string error,
		-- or return object with JSON data otherwise
		return err and err or obj
	else
		return nil
	end
end

--- Receives a request
-- Receives the user request and set it as an attribute of the object
function Controller:receive_request()
	self.request_get = request_get()
	self.request_post = request_post()
end

--- Sends a response
-- Sends a response to the user
function Controller:send_response()
	-- Generic validation
	local val = Validator.new(self.request_get, self.request_post)
	local status_data = { status = val:validate_request(), data = "" }
	-- If generic validation is ok, call the module controller
	if (status_data.status.code == STATUS.OK.code) then
		local api_module = self.request_get[1]
		local auth
		if(self.request_post ~= nil) then
			auth = AuthController.new(self.request_post[API_MODULES.AUTH])
		else
			auth = AuthController.new(self.request_post)
		end
		status_data = auth:do_action()
		if (status_data.status.code == STATUS.OK.code) then
			local amc
			if (api_module == API_MODULES.UCI) then
				if(self.request_post ~= nil) then
					amc = UCIController.new(self.request_get, self.request_post[API_MODULES.UCI])
				else
					amc = UCIController.new(self.request_get, self.request_post)
				end
			elseif (api_module == API_MODULES.SYSTEM) then
				amc = SystemController.new(self.request_get, self.request_post)
			end
			status_data = amc:do_action()
		end
	end
	-- Print HTTP Status header
	io.write("Status: " .. status_data.status.code .. " " .. status_data.status.message .. "\n")
	io.write("Access-Control-Allow-Origin:*" .. "\n")
	-- If everything is ok, encode data to JSON, prit an error otherwise
	if(status_data.status.code == STATUS.OK.code) then
		if (status_data.data ~= nil) then
			io.write("Content-Type: application/json" .. "\n\n")
			io.write(json.encode(status_data.data, { indent = true }))
		else
			io.write("\n\n")
		end
	else
		io.write("\n" .. status_data.status.code .. " " .. status_data.status.message)
		if (status_data.data ~= nil) then
			io.write(" " .. status_data.data)
		end
	end
end