--[[
lupus - Validator

Description:
Generic validation of request data

License:
Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--

local constants = require ("lupus.core.constants")

Validator = {}
Validator.__index = Validator

--- Validator constructor
-- Creates a new Validator
function Validator.new(request_get, request_post)
	local val = {}
	setmetatable(val, Validator)
	val.request_get = request_get
	val.request_post = request_post
	return val
end

--- Validate if requested api_module exists
-- @return true if action is valida, false otherwise
function Validator:validate_api_module()
	-- Try to find the first element of request_get into valid actions array
	for k, v in pairs(API_MODULES) do
		if (self.request_get[1] == v) then
			return true
		end
	end
	return false
end

--- Validate if JSON data is valid
-- Validate if request_post has an object with the JSON values.
-- @return true if JSON syntax is valid, false otherwise
function Validator:validate_JSON()
	-- request_post has been filled with an object, parsing JSON data
	-- if request_post is not an object or nil (there is no POST data),
	-- it means that an error has occurred and we have a string with this error
	return (type(self.request_post) == "table" or type(self.request_post) == "nil") and true or false
end

--- Receives a request
-- Receives the user request and set it as an attribute of the object
function Validator:validate_request()
	local vam = self:validate_api_module()
	if (not vam) then
		return STATUS.NOT_FOUND
	end
	local vj = self:validate_JSON()
	if (not vj) then
		return STATUS.BAD_REQUEST
	end
	return STATUS.OK
end