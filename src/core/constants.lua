API_MODULES = {
	UCI = "uci",
	SYSTEM = "system",
	AUTH = "auth"
}

STATUS = {
	OK           = {code = 200, message = "OK"},
 	BAD_REQUEST  = {code = 400, message = "Bad Request"},
 	UNAUTHORIZED = {code = 401, message = "Unauthorized"},
 	NOT_FOUND    = {code = 404, message = "Not found"},
	API_ERROR    = {code = 503, message = "Internal API error"}
}