--[[
lupus - AuthAction

Description:
Do the needed actions to build the desired response

License:
Copyright 2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--
--
local uci = require("uci")

AuthAction = {}
AuthAction.__index = AuthAction

--- Validator constructor
-- Creates a new Validator
function AuthAction.new(request_post)
	local act = {}
	setmetatable(act, AuthAction)
	act.request_post = request_post
	return act
end

--- Get authentication mode
function AuthAction:get_auth_mode()
	local cur = uci.cursor()
	local mode = cur:get("lupus", "auth", "mode")
	return mode
end

--- Plain mode authentication
-- Checks if sent user and passowrd match with credentials in config file
function AuthAction:password_mode()
	local status = STATUS.UNAUTHORIZED
	-- Get right credentials
	local cur = uci.cursor()
	local right_password = cur:get("lupus","auth","password")
	-- Traverse array to look for plain authentication credentials sent by the request
	local credentials = self.request_post
	local password
	if (credentials ~= nil) then
		for k, v in pairs(credentials) do
			if (k == "password") then
				password = v
			end
		end
	end
	-- Check if credentials match
	if (password == right_password) then
		status = STATUS.OK
	end
	return status
end