--[[
lupus - AuthController

Description:
Check if the request is authorized

License:
Copyright 2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--

local auth_action = require("lupus.modules.auth.AuthAction")
local constants = require ("lupus.modules.auth.Authconstants")

AuthController = {}
AuthController.__index = AuthController

--- Controller constructor
-- Creates a new Controller
function AuthController.new(request_post)
	local ctrl = {}
	setmetatable(ctrl, AuthController)
	ctrl.request_post = request_post
	return ctrl
end

--- Sends a response
-- Sends a response to the user
function AuthController:do_action()
	local action = AuthAction.new(self.request_post)
	local mode = action:get_auth_mode()
	local status = STATUS.UNAUTHORIZED
	if (mode == AUTH_MODE.NONE) then
		status = STATUS.OK
	elseif (mode == AUTH_MODE.PASSWORD) then
		status = action:password_mode()
	end
	return {status = status, data = nil}
end