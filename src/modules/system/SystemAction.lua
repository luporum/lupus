--[[
lupus - SystemAction

Description:
Do the needed actions to build the desired response

License:
Copyright 2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--

SystemAction = {}
SystemAction.__index = SystemAction

--- Validator constructor
-- Creates a new Validator
function SystemAction.new(request_get, request_post)
	local act = {}
	setmetatable(act, SystemAction)
	act.request_get = request_get
	act.request_post = request_post
	return act
end

--- Reboot the system
function SystemAction:reboot()
	local error = os.execute("reboot")
	if (error == SYSTEM_ERRORS.OK) then
		status = STATUS.OK
	else
		status = STATUS.API_ERROR
	end
	return {status = status, data = nil}
end

--- Restart a service
function SystemAction:service(action)
	local service = self.request_get[3]
	local status
	local error = os.execute("/etc/init.d/" .. service .. " " .. action)
	if (error == SYSTEM_ERRORS.OK) then
		status = STATUS.OK
	else
		status = STATUS.API_ERROR
	end
	return {status = status, data = nil}
end

--- Restart a service
function SystemAction:restart_service()
	return self:service("restart")
end

--- Start a service
function SystemAction:start_service()
	return self:service("start")
end

--- Stop a service
function SystemAction:stop_service()
	return self:service("stop")
end