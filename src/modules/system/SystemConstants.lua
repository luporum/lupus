SYSTEM_ACTIONS = {
	REBOOT = "reboot",
	RESTART_SERVICE = "restart_service",
	START_SERVICE = "start_service",
	STOP_SERVICE = "stop_service",
}

SYSTEM_ERRORS = {
	OK = 0
}