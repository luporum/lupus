--[[
lupus - SystemController

Description:
Receives a request from a CGI and sends a response

License:
Copyright 2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--
local system_validator = require("lupus.modules.system.SystemValidator")
local system_action = require("lupus.modules.system.SystemAction")

SystemController = {}
SystemController.__index = SystemController

--- Controller constructor
-- Creates a new Controller
function SystemController.new(request_get, request_post)
	local ctrl = {}
	setmetatable(ctrl,SystemController)
	ctrl.request_get = request_get
	ctrl.request_post = request_post
	return ctrl
end

--- Sends a response
-- Sends a response to the user
function SystemController:do_action()
	-- Validate system request data
	local val = SystemValidator.new(self.request_get, self.request_post)
	local status_data = {status = val:validate_request(), data = ""}
	-- If everything is ok, do the corresponding action
	if(status_data.status.code == STATUS.OK.code) then
		local acts = self.request_get[2]
		local action = SystemAction.new(self.request_get, self.request_post)
		if (acts == SYSTEM_ACTIONS.REBOOT) then
			status_data = action:reboot()
		elseif (acts == SYSTEM_ACTIONS.RESTART_SERVICE) then
			status_data = action:restart_service()
		elseif (acts == SYSTEM_ACTIONS.START_SERVICE) then
			status_data = action:start_service()
		elseif (acts == SYSTEM_ACTIONS.STOP_SERVICE) then
			status_data = action:stop_service()
		end
	end
	return status_data
end