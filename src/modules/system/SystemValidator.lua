--[[
lupus - SystemValidator

Description:
Validate request data

License:
Copyright 2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--

local lfs = require("lfs")
local constants = require ("lupus.modules.system.SystemConstants")

SystemValidator = {}
SystemValidator.__index = SystemValidator

--- Validator constructor
-- Creates a new Validator
function SystemValidator.new(request_get, request_post)
	local val = {}
	setmetatable(val, SystemValidator)
	val.request_get = request_get
	val.request_post = request_post
	return val
end

--- Validate if requested action exists
-- @return true if action is valida, false otherwise
function SystemValidator:validate_action()
	-- Try to find the first element of request_get into valid actions array
	local actions = SYSTEM_ACTIONS
	for k, v in pairs(actions) do
		if (self.request_get[2] == v) then
			return true
		end
	end
	return false
end

--- Validate number of parameters
-- Validate if number of parameters i right depending on the requested action
-- @return true if action is valida, false otherwise
function SystemValidator:validate_params()
	local action = self.request_get[2]
	local nparams = table.maxn(self.request_get)
	if (action == SYSTEM_ACTIONS.REBOOT and nparams == 2) then
		return true
	elseif ((action == SYSTEM_ACTIONS.RESTART_SERVICE or
		     action == SYSTEM_ACTIONS.START_SERVICE   or
		     action == SYSTEM_ACTIONS.STOP_SERVICE)   and
		    nparams == 3) then
		return self:validate_service()
	end
	return false
end

--- Validate if a service exists
-- Validate if a service exists, checking if the corresponding 
-- init script in /etc/init.d exists
-- @return true if init scripts dir exists and service exist, false otherwise
function SystemValidator:validate_service()
	local service = self.request_get[3]
	local init_dir = "/etc/init.d"
	-- Check if init scripts dir exists
	if (not file_exists(init_dir)) then
		return false
	end
	-- Check if requested service exists
	for file in lfs.dir(init_dir) do
		if (file == service) then
			return true
		end
	end
	return false
end

--- Receives a request
-- Receives the user request and set it as an attribute of the object
function SystemValidator:validate_request()
	local va = self:validate_action()
	if (not va) then
		return STATUS.NOT_FOUND
	end
	local va = self:validate_params()
	if (not va) then
		return STATUS.BAD_REQUEST
	end
	return STATUS.OK
end