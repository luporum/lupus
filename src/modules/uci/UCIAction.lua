--[[
lupus - Action

Description:
Do the needed actions to build the desired response

License:
Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--

local uci = require("uci")
local lfs = require("lfs")
local strings = require("lupus.utils.strings")
local files = require("lupus.utils.files")
local uciutils = require("lupus.modules.uci.uciutils")

UCIAction = {}
UCIAction.__index = UCIAction

--- Validator constructor
-- Creates a new Validator
function UCIAction.new(request_get, request_post)
	local act = {}
	setmetatable(act, UCIAction)
	act.request_get = request_get
	act.request_post = request_post
	return act
end

--- Add an anonymous section of type section_type to the given configuration.
-- POST request has this format
-- { "config1.section_type1",
--   "config2.section_type2",
--   ... }
-- but has been previously decoded to an array.
function UCIAction:add()
	local cur = uci.cursor()
	local sections = self.request_post
	local status
	-- Traverse array with add actions
	for _, v in ipairs(sections) do
		local config_type = strings.split_char(v, ".")
		local config = config_type[1]
		local type = config_type[2]
		-- Add section
		if (cur:add(config, type)) then
			status = STATUS.OK
			cur:commit(config)
		else
			-- If something goes wrong, exit
			status = STATUS.BAD_REQUEST
			break
		end
	end
	return {status = status, data = nil}
end

--- Delete the given section or option
-- POST request has one the following formats
-- { "config1.section",
--   "config2.section.option",
--   ... }
-- but has been previously decoded to an array.
function UCIAction:delete()
	local cur = uci.cursor()
	local sections = self.request_post
	local status
	-- TODO: It is not working for more than one delete action
	-- Traverse array with add actions
	for _, v in ipairs(sections) do
		local config_section_option = strings.split_char(v, ".")
		local config = config_section_option[1]
		local section = config_section_option[2]
		section = uciutils.get_section_name(config, section)
		local option = config_section_option[3] -- this may be nil
		-- Delete section or option
		if (option == nil and cur:delete(config, section) or
		option ~= nil and cur:delete(config, section, option)) then
			status = STATUS.OK
			cur:commit(config)
		else
			-- If something goes wrong, exit
			status = STATUS.BAD_REQUEST
			break
		end
	end
	return {status = status, data = nil}
end

--- Get the value of the given option or the type of the given section.
-- The request has one of the following formats:
-- 1) config.section.option	In this case, we'll return the option value
-- 2) config.section		In this case, we'll return the section type
-- 'section' can be the section name or an indexed type with the format @type[index]
--
-- The method creates a table with status and data.
-- This table will have one of the following formats:
-- 1) {status = status_number, data = { config.section.option = option_value } }
-- 2) {status = status_number, data = { config.section = type } }
--
-- @return a table with status and data
function UCIAction:get()
	-- UCI path we want to get
	local get_path = self.request_get[3]
	-- get_path has the following format: config.section.path, split it
	local config_section_option = strings.split_char(get_path,".")
	-- Get config, section and option
	local config = config_section_option[1]
	local section = config_section_option[2]
	-- get sectin name, this field could be an indexed type instead of the sectin name
	section = uciutils.get_section_name(config, section)
	local option = config_section_option[3]
	-- Get option value and return a table with get path and option value
	local status_data = {}
	local t = {}
	local cur = uci.cursor()
	if (section ~= nil) then
		if (option == nil) then
			-- if there is no option, get the type
			t[get_path] = uciutils.get_section_type(config, section)
		else
			-- if there is an option, get the option value
			t[get_path] = cur:get(config, section, option)
		end
		if (t[get_path] == nil) then
			status_data = {status = STATUS.NOT_FOUND, data = ""}
		else
			status_data = {status = STATUS.OK, data = t}
		end
	else
		status_data = {status = STATUS.NOT_FOUND, data = ""}
	end
	return status_data
end

--- Get all UCI structure
-- Creates a table with status and data.
-- If request is of type /uci/get_all/config, creates a table with its sections
-- and options of config.
-- If request is of type /uci/get_all, creates a table with all configs, with
-- their sections and options.
-- The final table is
-- {status = status_number, data = { table_with_configs_sections_options } }
-- @return a table with status and data
function UCIAction:get_all()
	local config = self.request_get[3]
	local status_data = {}
	local t = {}
	local cur = uci.cursor()
	-- Check if get_all has a parameter asking for a config
	if (config ~= nil) then
		-- If there is a parameter, get all info of this config
		t = cur:get_all(config)
		if (t == nil) then
			status_data = {status = STATUS.NOT_FOUND, data = ""}
		else
			status_data = {status = STATUS.OK, data = t}
		end
	else
		-- If there is no parameter, get all configs
		-- and build a table with all sections of all configs
		local status_data_configs = self:get_configs()
		if (status_data_configs.status.code ~= STATUS.OK.code) then
			status_data = status_data_configs
		else
			-- Build the table
			for _, config in pairs(status_data_configs.data.configs) do
				t[config] = cur:get_all(config)
			end
			status_data = {status = STATUS.OK, data = t}
		end
	end
	return status_data
end

--- Get all configurable configs
-- Creates a table with status and data. Data will be a table with all filenames that are in /etc/config.
-- This tables has this format:
-- { configs = {filename1, filename2,...} }
-- The final table is
-- {status = status_number, data = { configs = {filename1, filename2,...} } }
-- @return a table with status and data
function UCIAction:get_configs()
	local tconfigs = {}
	local taux = {}
	local status_data = {}
	local config_dir = "/etc/config"
	-- Check if config dir exists, if not return 503
	if (not file_exists(config_dir)) then
		return {status = STATUS.API_ERROR, data = ""}
	end
	-- Get all filenames of config directory
	for file in lfs.dir(config_dir) do
		if(file ~= "." and file ~= "..") then
			table.insert(taux,file)
		end
	end
	tconfigs.configs = taux
	status_data = {status = STATUS.OK, data = tconfigs}
	return status_data
end

--- Set the value of the given options, or add new sections with the type set to the given value.
-- Set all requested option values and all sections contained in POST request.
-- POST request has this format
-- { "config1.sectionname1.option1":"value1",
--   "config2.type2":"sectionname2",
--   ... }
-- but has been previously decoded to an array.
function UCIAction:set()
	local cur = uci.cursor()
	local options = self.request_post
	local status
	-- Traverse array with set actions
	for k, v in pairs(options) do
		local set_params = strings.split_char(k, ".")
		local config = set_params[1]
		local section = set_params[2]
		local option = set_params[3]
		local value = v
		local ok
		if (option == nil) then
			-- Add a section of type "section" with name value
			ok = cur:set(config, value, section)
		else
			section = uciutils.get_section_name(config, section)
			-- Set value
			ok = cur:set(config, section, option, value)
		end
		if (ok) then
			status = STATUS.OK
			cur:commit(config)
		else
			-- If something goes wrong, exit, any set action will be done
			status = STATUS.BAD_REQUEST
			break
		end
	end
	return {status = status, data = nil}
end