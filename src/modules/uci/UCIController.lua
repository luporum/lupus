--[[
lupus - UCIController

Description:
Receives a request from a CGI and sends a reponse

License:
Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--
local uci_validator = require("lupus.modules.uci.UCIValidator")
local uci_action = require("lupus.modules.uci.UCIAction")

UCIController = {}
UCIController.__index = UCIController

--- Controller constructor
-- Creates a new Controller
function UCIController.new(request_get, request_post)
	local ctrl = {}
	setmetatable(ctrl,UCIController)
	ctrl.request_get = request_get
	ctrl.request_post = request_post
	return ctrl
end

--- Sends a response
-- Sends a response to the user
function UCIController:do_action()
	-- Validate UCI request data
	local val = UCIValidator.new(self.request_get, self.request_post)
	local status_data = {status = val:validate_request(), data = ""}
	-- If everything is ok, do the corresponding action
	if(status_data.status.code == STATUS.OK.code) then
		local acts = self.request_get[2]
		local action = UCIAction.new(self.request_get, self.request_post)
		if (acts == UCI_ACTIONS.GET_CONFIGS) then
			status_data = action:get_configs()
		elseif (acts == UCI_ACTIONS.GET) then
			status_data = action:get()
		elseif (acts == UCI_ACTIONS.GET_ALL) then
			status_data = action:get_all()
		elseif (acts == UCI_ACTIONS.SET) then
			status_data = action:set()
		elseif (acts == UCI_ACTIONS.ADD) then
			status_data = action:add()
		elseif (acts == UCI_ACTIONS.DELETE) then
			status_data = action:delete()
		end
	end
	return status_data
end