--[[
lupus - UCIValidator

Description:
Validate request data

License:
Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--

local constants = require ("lupus.modules.uci.UCIconstants")
local strings = require("lupus.utils.strings")

UCIValidator = {}
UCIValidator.__index = UCIValidator

--- Validator constructor
-- Creates a new Validator
function UCIValidator.new(request_get, request_post)
	local val = {}
	setmetatable(val, UCIValidator)
	val.request_get = request_get
	val.request_post = request_post
	return val
end

--- Validate if requested action exists
-- @return true if action is valida, false otherwise
function UCIValidator:validate_action()
	-- Try to find the first element of request_get into valid actions array
	local actions = UCI_ACTIONS
	for k, v in pairs(actions) do
		if (self.request_get[2] == v) then
			return true
		end
	end
	return false
end

--- Validate number of parameters
-- Validate if number of parameters i right depending on the requested action
-- @return true if action is valida, false otherwise
function UCIValidator:validate_params()
	local action = self.request_get[2]
	local nparams = table.maxn(self.request_get)
	if (action == UCI_ACTIONS.GET_ALL and (nparams == 2 or nparams == 3)) then
		return true
	elseif (action == UCI_ACTIONS.GET_CONFIGS and nparams == 2) then
		return true
	elseif (action == UCI_ACTIONS.GET and nparams == 3) then
		return self:validate_params_get()
	elseif (action == UCI_ACTIONS.SET and nparams == 2) then
		return self:validate_params_set()
	elseif (action == UCI_ACTIONS.ADD and (nparams == 2 or nparams == 3)) then
		return self:validate_params_add()
	elseif (action == UCI_ACTIONS.DELETE and nparams == 2) then
		return true
		-- TODO: Implement validate_params_delete
	end
	return false
end

--- Validate if a request of type /uci/get/... is valid
-- @return true if the request is valid, false otherwise
function UCIValidator:validate_params_get()
	local get_path = self.request_get[3]
	-- Accept option paths (config.section.option) and section paths (config.section)
	return self:validate_option_path(get_path) or
		   self:validate_section_path(get_path)
end

--- Validate if a request of type /uci/set is valid
-- @return true if the request is valid, false otherwise
function UCIValidator:validate_params_set()
	local set_req = self.request_post
	for k, v in pairs(set_req) do
		-- Check if each key is a valid option path
		if (not self:validate_option_path(k) and not self:validate_section_path(k)) then
			return false
		end
	end
	return true
end

--- Validate if a request of type /uci/add is valid
-- @return true if the request is valid, false otherwise
function UCIValidator:validate_params_add()
	local add_req = self.request_post
	for _, v in ipairs(add_req) do
		-- Check if each key is a valid option path
		if (not self:validate_section_path(v)) then
			return false
		end
	end
	return true
end

--- Validate if an option path is valid
-- an option path must have one of the following formats:
-- * config.section_name.option
-- * config.@interface_name[index].option
-- config, section_name, interface_name and option must be valid identifiers
-- (they may only contain the characters a-z, 0-9 and _.)
-- @return true if the option path is valid, false otherwise
function UCIValidator:validate_option_path(option_path)
	-- Check if option_path has the following format: config.section_or_type.option
	-- UCI identifiers may only contain the characters a-z, 0-9 and _
	if (option_path:match("^[a-z0-9_]+%.[^%.]+%.[a-z0-9_]+$") == nil) then
		return false
	end
	-- If section is requested via indexed type, check if it has the following format @typename[number]
	local section_type = strings.split_char(option_path, ".")[2]
	if (string.sub(section_type,1,1) == "@") then
		if (section_type:match("^@[a-z0-9_]+%[[0-9]+%]$") == nil) then
			return false
		end
	else
		-- If it is a section name, check if it is a valid identifier
		if (section_type:match("^[a-z0-9_]+$") == nil) then
			return false
		end
	end
	return true
end

--- Validate if a section path is valid
-- A section path must have the following format:
-- * config.section
--
-- 'section' can be a section name or an indexed type
-- Both section name and type must be valid identifiers
-- (they may only contain the characters a-z, 0-9 and _.)
--
-- @return true if the section path is valid, false otherwise
function UCIValidator:validate_section_path(section_path)
		-- Check if option_path has the following format: config.section_or_type.option
	-- UCI identifiers may only contain the characters a-z, 0-9 and _
	if (section_path:match("^[a-z0-9_]+%.[^%.]+$") == nil) then
		return false
	end
	-- If section is requested via indexed type, check if it has the following format @typename[number]
	local section_type = strings.split_char(section_path, ".")[2]
	if (string.sub(section_type,1,1) == "@") then
		if (section_type:match("^@[a-z0-9_]+%[[0-9]+%]$") == nil) then
			return false
		end
	else
		-- If it is a section name, check if it is a valid identifier
		if (section_type:match("^[a-z0-9_]+$") == nil) then
			return false
		end
	end
	return true
end

--- Receives a request
-- Receives the user request and set it as an attribute of the object
function UCIValidator:validate_request()
	local va = self:validate_action()
	if (not va) then
		return STATUS.NOT_FOUND
	end
	local va = self:validate_params()
	if (not va) then
		return STATUS.BAD_REQUEST
	end
	return STATUS.OK
end