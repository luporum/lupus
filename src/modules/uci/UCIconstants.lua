UCI_ACTIONS = {
	ADD = "add",
	DELETE = "delete",
	GET_ALL = "get_all",
	GET_CONFIGS = "get_configs",
	GET = "get",
	SET = "set"
}