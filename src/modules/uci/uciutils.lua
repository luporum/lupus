--[[
lupus - uciutils

Description:
UCI Util functions

License:
Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--

local strings = require("lupus.utils.strings")
local uci = require("uci")

local uciutils = {}

--- Check if a identifier is a string (section name) or an indexed type.
-- @param identifier a string
-- @return section name if it is a normal string,
--         or type and index if it is an indexed type
function uciutils.get_section_type_index(identifier)
	if (identifier:sub(1,1) == "@") then
		-- Get type name and index
		local type = strings.split_char(identifier,"[")[1]
		type = type:sub(2)
		local index = strings.split_char(identifier, "[")[2]
		index = tonumber(index:sub(1, -2))
		return type, index
	else
		return identifier
	end
end

--- Get the section name from a section identifier. 
-- If the identifier is a section name, it will be returned as it is, 
-- if it is an indexed type, the hidden section name will be returned.
-- @param identifier a string
-- @return section name if it is a normal string,
--         the hidden name if it is an indexed type
--         or nil if the index is wrong
function uciutils.get_section_name(config, identifier)
	if (identifier:sub(1,1) == "@") then
		local type, index = uciutils.get_section_type_index(identifier)
		local cur = uci.cursor()
		local i = 0
		local hidden_name
		cur:foreach(config, type,
			function(s)
				-- Get the name for the required index
				if (i == index) then
					hidden_name = s['.name']
				end
				i = i + 1
			end)
		return hidden_name
	else
		return identifier
	end
end

--- Get the section type from a section identifier.
-- If the identifier is a section name, its type will be searched and returned,
-- if it is an indexed type, the type name will be returned.
-- @param identifier a string
-- @return the section type name
function uciutils.get_section_type(config, identifier)
	local type
	if (identifier:sub(1,1) == "@") then
		-- If identifier is an indexed type, get type name
		type = uciutils.get_section_type_index(identifier)
	else
		-- If identifier is a section name, search type
		local section = identifier
		local cur = uci.cursor()
		-- Get all information about this config
		local all = cur:get_all(config)
		-- Get the type of this sections name
		type = all[section]['.type']
	end
	return type
end

return uciutils