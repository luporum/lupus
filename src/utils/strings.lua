--[[
lupus - strings

Description:
Util functions for strings

License:
Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>

This file is part of lupus.

lupus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

lupus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with lupus.  If not, see <http://www.gnu.org/licenses/>.

]]--

local strings = {}

--- Split a string by a delimiter char
-- @param str the string to be split
-- @param delimiter the delimiter character
-- @return a table with strings delimited by the char
function strings.split_char(str, delimiter)
	local t = {}
	local i = 1
	if(str ~= nil) then
		for v in str:gmatch("[^" .. delimiter .. "]+") do
			t[i] = v
			i = i + 1
		end
	end
	return t
end

return strings