#!/bin/bash

# lupus - test_add.sh
#
# Description:
# Tests for get action
#
# License:
# Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>
#
# This file is part of lupus.
#
# lupus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lupus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lupus.  If not, see <http://www.gnu.org/licenses/>.

tests_add() {
	tests_title "/uci/add"

	# Add a section
	local data="{\"$TEST_CONFIG_FILE.testadd\"}"
	local actual="`curl -s -d $data -o /dev/null -w %{http_code} $ROOT_URL/uci/add`"
	local expected='200'
	assert_equals "$actual" "$expected"

	# Add some sections
	data="{\"$TEST_CONFIG_FILE.testadd1\",\"$TEST_CONFIG_FILE.testadd2\"}"
	actual="`curl -s -d $data -o /dev/null -w %{http_code} $ROOT_URL/uci/add`"
	expected='200'
	assert_equals "$actual" "$expected"

	# Check if first section is added, setting an option
	curl -s -d "{\"$TEST_CONFIG_FILE.testadd2\"}" -o /dev/null $ROOT_URL/uci/add
	curl -s -d "{\"$TEST_CONFIG_FILE.@testadd2[0].option1\":\"value1\"}" -o /dev/null $ROOT_URL/uci/set
	actual="`curl --globoff -s $ROOT_URL/uci/get/$TEST_CONFIG_FILE.@testadd2[0].option1|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.@testadd2[0].option1\":\"value1\"}"
	assert_equals "$actual" "$expected"

}
