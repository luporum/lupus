#!/bin/bash

# lupus - test_all.sh
#
# Description:
# Launch all API tests
#
# License:
# Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>
#
# This file is part of lupus.
#
# lupus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lupus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lupus.  If not, see <http://www.gnu.org/licenses/>.

source test_env.sh
source test_functions.sh
source test_add.sh
source test_delete.sh
source test_generic.sh
source test_get.sh
source test_get_configs.sh
source test_set.sh

source test_service.sh

HOST="192.168.1.5"
USER="root"
ROOT_URL="http://$HOST/cgi-bin/lupus"
TEST_CONFIG_FILE="testlupus"
COUNT=0
COUNT_PASSED=0

# uci module tests

copy_test_config_file
tests_add
copy_test_config_file
tests_delete
copy_test_config_file
tests_generic
copy_test_config_file
tests_get
remove_test_config_file
tests_get_configs
copy_test_config_file
tests_set
remove_test_config_file

# system module tests

tests_service

echo -e "\nTESTS PASSED: $COUNT_PASSED/$COUNT"