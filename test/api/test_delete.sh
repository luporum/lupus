#!/bin/bash

# lupus - test_delete.sh
#
# Description:
# Tests for delete action
#
# License:
# Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>
#
# This file is part of lupus.
#
# lupus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lupus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lupus.  If not, see <http://www.gnu.org/licenses/>.

tests_delete() {
	tests_title "/uci/delete"

	# Delete a section
	local data="{\"$TEST_CONFIG_FILE.@interface[0]\"}"
	local actual="`curl -s -d $data -o /dev/null -w %{http_code} $ROOT_URL/uci/delete`"
	local expected='200'
	assert_equals "$actual" "$expected"

	# TODO: Add test for more than one section

	# Delete an option
	data="{\"$TEST_CONFIG_FILE.lan.ipaddr\"}"
	actual="`curl -s -d $data -o /dev/null -w %{http_code} $ROOT_URL/uci/delete`"
	expected='200'
	assert_equals "$actual" "$expected"

}
