#!/bin/bash

# lupus - test_functions.sh
#
# Description:
# Useful functions for tests
#
# License:
# Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>
#
# This file is part of lupus.
#
# lupus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lupus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lupus.  If not, see <http://www.gnu.org/licenses/>.

# Compare if first and second parameters are equal
# It also prints the test number
assert_equals() {
	local actual=$1
	local expected=$2
	echo -n "TEST $COUNT: "
	[ "$actual" == "$expected" ] && { echo "OK"; COUNT_PASSED=$(($COUNT_PASSED+1)); } || echo "KO"
	COUNT=$(($COUNT+1))
}

# Print a title for each group of tests
tests_title() {
	echo -e "\nTESTS: $1"
	local n_chars=`echo TESTS: $1|wc -c`
	for i in `seq 2 $n_chars`; do echo -n "="; done
	echo ""
}