#!/bin/bash

# lupus - test_generic.sh
#
# Description:
# Generic tests
#
# License:
# Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>
#
# This file is part of lupus.
#
# lupus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lupus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lupus.  If not, see <http://www.gnu.org/licenses/>.

tests_generic() {
	tests_title "generic"
	
	# Invalid module
	local actual="`curl -s --globoff -o /dev/null -w %{http_code} $ROOT_URL/uci/lala|tr -d " "|tr -d "\n"`"
	local expected='404'
	assert_equals "$actual" "$expected"
	
	# Invalid module and more
	actual="`curl -s --globoff -o /dev/null -w %{http_code} $ROOT_URL/uci/lala/lele|tr -d " "|tr -d "\n"`"
	expected='404'
	assert_equals "$actual" "$expected"
	
	# Valid JSON
	data="{\"a\":\"b\"}"
	actual="`curl -s -d $data -o /dev/null -w %{http_code} $ROOT_URL/uci/get/$TEST_CONFIG_FILE.lan.ipaddr`"
	expected='200'
	assert_equals "$actual" "$expected"
	
	# Invalid JSON
	data="a"
	actual="`curl -s -d $data -o /dev/null -w %{http_code} $ROOT_URL/uci/get/$TEST_CONFIG_FILE.lan.ipaddr`"
	expected='400'
	assert_equals "$actual" "$expected"
	
}
