#!/bin/bash

# lupus - test_get.sh
#
# Description:
# Tests for get action
#
# License:
# Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>
#
# This file is part of lupus.
#
# lupus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lupus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lupus.  If not, see <http://www.gnu.org/licenses/>.

tests_get() {
	tests_title "/uci/get"
	
	# Get by section name, output is a single value
	local actual="`curl -s $ROOT_URL/uci/get/$TEST_CONFIG_FILE.loopback.ipaddr|tr -d " "|tr -d "\n"`"
	local expected="{\"$TEST_CONFIG_FILE.loopback.ipaddr\":\"127.0.0.1\"}"
	assert_equals "$actual" "$expected"
	
	# Get by section name, output is a list
	actual="`curl -s $ROOT_URL/uci/get/$TEST_CONFIG_FILE.ntp.server|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.ntp.server\":[\"0.openwrt.pool.ntp.org\",\"1.openwrt.pool.ntp.org\",\"2.openwrt.pool.ntp.org\",\"3.openwrt.pool.ntp.org\"]}"
	assert_equals "$actual" "$expected"
	
	# Get by indexed interface, output is a single value
	actual="`curl -s --globoff $ROOT_URL/uci/get/$TEST_CONFIG_FILE.@interface[0].ipaddr|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.@interface[0].ipaddr\":\"127.0.0.1\"}"
	assert_equals "$actual" "$expected"
	
	actual="`curl -s --globoff $ROOT_URL/uci/get/$TEST_CONFIG_FILE.@system[0].hostname|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.@system[0].hostname\":\"OpenWrt\"}"
	assert_equals "$actual" "$expected"
	
	# Get section type
	actual="`curl -s --globoff $ROOT_URL/uci/get/$TEST_CONFIG_FILE.loopback|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.loopback\":\"interface\"}"
	assert_equals "$actual" "$expected"
	
	actual="`curl -s --globoff $ROOT_URL/uci/get/$TEST_CONFIG_FILE.@interface[0] |tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.@interface[0]\":\"interface\"}"
	assert_equals "$actual" "$expected"
	
	# No params for get
	actual="`curl -s -o /dev/null -w %{http_code} $ROOT_URL/uci/get|tr -d " "|tr -d "\n"`"
	expected='400'
	assert_equals "$actual" "$expected"
	
	# Only 1 param
	actual="`curl -s -o /dev/null -w %{http_code} $ROOT_URL/uci/get/$TEST_CONFIG_FILE|tr -d " "|tr -d "\n"`"
	expected='400'
	assert_equals "$actual" "$expected"
	
	#Invalid params
	actual="`curl -s -o /dev/null -w %{http_code} $ROOT_URL/uci/get/s.s.s|tr -d " "|tr -d "\n"`"
	expected='404'
	assert_equals "$actual" "$expected"
	
}
