#!/bin/bash

# lupus - test_get_configs.sh
#
# Description:
# Tests for get_configs action
#
# License:
# Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>
#
# This file is part of lupus.
#
# lupus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lupus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lupus.  If not, see <http://www.gnu.org/licenses/>.

tests_get_configs() {
	tests_title "/uci/get_configs"
	
	# Get configs, only works ina default OpenWRT installation
	local actual="`curl -s $ROOT_URL/uci/get_configs|tr -d " "|tr -d "\n"`"
	local expected='{"configs":["luci","network","firewall","dhcp","system","ucitrack","uhttpd","dropbear"]}'
	assert_equals "$actual" "$expected"
}
