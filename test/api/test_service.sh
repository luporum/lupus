#!/bin/bash

# lupus - test_service.sh
#
# Description:
# Tests for managing services (restart, start, stop)
#
# License:
# Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>
#
# This file is part of lupus.
#
# lupus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lupus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lupus.  If not, see <http://www.gnu.org/licenses/>.

tests_service() {
	tests_title "/system/[restart|start|stop]_service"

	# Restart a service
	local actual="`curl -s -o /dev/null -w %{http_code} $ROOT_URL/system/restart_service/firewall`"
	local expected='200'
	assert_equals "$actual" "$expected"

	# Stop a service
	local actual="`curl -s -o /dev/null -w %{http_code} $ROOT_URL/system/stop_service/firewall`"
	local expected='200'
	assert_equals "$actual" "$expected"

	# Start a service
	local actual="`curl -s -o /dev/null -w %{http_code} $ROOT_URL/system/start_service/firewall`"
	local expected='200'
	assert_equals "$actual" "$expected"

	# Try to restart an invalid service
	local actual="`curl -s -o /dev/null -w %{http_code} $ROOT_URL/system/restart_service/notaservice`"
	local expected='400'
	assert_equals "$actual" "$expected"
}
