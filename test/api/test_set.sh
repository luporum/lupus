#!/bin/bash

# lupus - test_set.sh
#
# Description:
# Tests for set action
#
# License:
# Copyright 2013-2014 Mònica Ramírez Arceda <monica@probeta.net>
#
# This file is part of lupus.
#
# lupus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# lupus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with lupus.  If not, see <http://www.gnu.org/licenses/>.

tests_set() {
	tests_title "/uci/set"
	
	# Set single value with a section name
	local data="{\"$TEST_CONFIG_FILE.loopback.ipaddr\":\"127.0.0.5\"}"
	local actual="`curl -s -d $data -o /dev/null -w %{http_code} $ROOT_URL/uci/set`"
	local expected='200'
	assert_equals "$actual" "$expected"
	actual="`curl -s $ROOT_URL/uci/get/$TEST_CONFIG_FILE.loopback.ipaddr|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.loopback.ipaddr\":\"127.0.0.5\"}"
	assert_equals "$actual" "$expected"
	
	# Set single value with a section name
	data="{\"$TEST_CONFIG_FILE.@system[0].hostname\":\"OpenWrta\"}"
	actual="`curl -s -d $data -o /dev/null -w %{http_code} $ROOT_URL/uci/set`"
	expected='200'
	assert_equals "$actual" "$expected"
	actual="`curl --globoff -s $ROOT_URL/uci/get/$TEST_CONFIG_FILE.@system[0].hostname|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.@system[0].hostname\":\"OpenWrta\"}"
	assert_equals "$actual" "$expected"
		
	# Set single a list
	data="{\"$TEST_CONFIG_FILE.ntp.server\":[\"4.openwrt.pool.ntp.org\",\"5.openwrt.pool.ntp.org\"]}"
	actual="`curl -s -d $data -o /dev/null -w %{http_code} $ROOT_URL/uci/set`"
	expected='200'
	assert_equals "$actual" "$expected"
	actual="`curl -s $ROOT_URL/uci/get/$TEST_CONFIG_FILE.ntp.server|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.ntp.server\":[\"4.openwrt.pool.ntp.org\",\"5.openwrt.pool.ntp.org\"]}"
	assert_equals "$actual" "$expected"
	
	# Add a section
	data="{\"$TEST_CONFIG_FILE.interface\":\"wlan0\"}"
	actual="`curl -s -d $data -o /dev/null -w %{http_code} $ROOT_URL/uci/set`"
	expected='200'
	actual="`curl -s $ROOT_URL/uci/get/$TEST_CONFIG_FILE.wlan0|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.wlan0\":\"interface\"}"
	assert_equals "$actual" "$expected"

	# Set multiple values
	data="{\"$TEST_CONFIG_FILE.loopback.ipaddr\":\"127.0.0.15\", \"$TEST_CONFIG_FILE.@system[0].hostname\":\"OpenWrtab\",\"$TEST_CONFIG_FILE.ntp.server\":[\"10.openwrt.pool.ntp.org\",\"11.openwrt.pool.ntp.org\",\"12.openwrt.pool.ntp.org\"]}"
	actual="`curl -s -d "$data" -o /dev/null -w %{http_code} $ROOT_URL/uci/set`"
	expected='200'
	assert_equals "$actual" "$expected"
	actual="`curl -s $ROOT_URL/uci/get/$TEST_CONFIG_FILE.loopback.ipaddr|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.loopback.ipaddr\":\"127.0.0.15\"}"
	assert_equals "$actual" "$expected"
	actual="`curl --globoff -s $ROOT_URL/uci/get/$TEST_CONFIG_FILE.@system[0].hostname|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.@system[0].hostname\":\"OpenWrtab\"}"
	assert_equals "$actual" "$expected"
	actual="`curl -s $ROOT_URL/uci/get/$TEST_CONFIG_FILE.ntp.server|tr -d " "|tr -d "\n"`"
	expected="{\"$TEST_CONFIG_FILE.ntp.server\":[\"10.openwrt.pool.ntp.org\",\"11.openwrt.pool.ntp.org\",\"12.openwrt.pool.ntp.org\"]}"
	assert_equals "$actual" "$expected"
}
